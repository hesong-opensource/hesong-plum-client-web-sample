# Hesong Plum Call-Center System 桌面客户端 Local WebAPI Sample

## CONTRIBUTING

1. 安装 Nodejs 6.0+
1. 安装 gulp:

    npm install -g gulp-cli

1. 安装 bower

    npm install -g bower

1. 安装这个 Sample 其它依赖包

    npm install

1. 启动 Gulp 内置 Web 服务器

    gulp serve
