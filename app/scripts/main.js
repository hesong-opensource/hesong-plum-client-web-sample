var ws;

$("[href='#init'").click(() => {
    let url = 'ws:127.0.0.1:8430'
    console.log(`连接 ${url} ...`);
    ws = new WebSocket(url);
    ws.onclose = (ev) => {
        console.warn(`${ws.url} onclose: code=${ev.code} reason=${ev.reason}`);
    };
    ws.onerror = () => {
        console.error(`${ws.url} onerror`);
    };
    ws.onopen = () => {
        console.info(`${ws.url} onopen`);
        ws.send(JSON.stringify({
            jsonrpc: "2.0",
            id: UUID.generate(),
            method: "getAgentState",
            params: []
        }));
    };
    ws.onmessage = (ev) => {
        let data = JSON.parse(ev.data);
        if ('result' in data) {
            console.info('RPC 返回: ', data);
        } else if ('error' in data) {
            console.error('RPC 错误: ', data);
        } else if ('method' in data) {
            console.log('RPC 事件: ', data);
        } else {
            console.warn('RPC 未知数据: ', data);
        }
    };
});


$("[href='#echo'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "echo",
        params: { message: "Hello" }
    }));
});


$("[href='#showLoginBox'").click(() => {
    let userName = $("#userName").val();
    let password = $("#password").val();
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "showLoginBox",
        params: [userName, password]
    }));
});

$("[href='#get'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "get",
        params: []
    }));
});

$("[href='#logout'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "logout",
        params: []
    }));
});

$("[href='#setAgentStatus-idle'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "setAgentStatus",
        params: [3]
    }));
});


$("[href='#setAgentStatus-away'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "setAgentStatus",
        params: [4]
    }));
});

$("[href='#pinWindow-true'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "pinWindow",
        params: [true]
    }));
});

$("[href='#pinWindow-false'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "pinWindow",
        params: [false]
    }));
});

$("[href='#pickUp'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "pickUp",
        params: []
    }));
});

$("[href='#pickDown'").click(() => {
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "pickDown",
        params: []
    }));
});

$("[href='#callOut'").click(() => {
    let calloutNum = $("#callOutNum").val();
    ws.send(JSON.stringify({
        jsonrpc: "2.0",
        id: UUID.generate(),
        method: "callOut",
        params: [calloutNum]
    }));
});
